const { src, dest, series, watch } = require('gulp');
const config = require('../config');
const imgIcons = config.src.img + 'icons/*.*';
const imgSvg = config.src.img + 'svg/*.*';
const newer = require('gulp-newer');
const browserSync = require("browser-sync");
const responsive = require('gulp-responsive');
const imageSize = 575;

// copy static files and optimize images
function copy(done) {
	// copy and optimize all images except icons and svg
	src([config.src.img + '**/*.{jpg,png,gif,webp}', '!' + imgIcons, '!' + imgSvg])
		.pipe(newer(config.dest.img))
		.pipe(responsive({
			'**/*.{jpg,png,gif,webp}': [
				{
					width: imageSize,
					withoutEnlargement: true,
					skipOnSmaller: true,
					skipOnEnlargement: true,
					quality: 95,
					rename: { suffix: '-'+imageSize+'px' },
				},
			],
		}, {
			errorOnEnlargement: false,
			withMetadata: false,
		}))
		.pipe(dest(config.dest.img));

	// copy original images (jpg, webp, png, gif) from src.img to dist
	src([config.src.img + '**/*.{jpg,png,gif,webp}', '!' + imgIcons, '!' + imgSvg])
		.pipe(newer(config.dest.img))
		.pipe(dest(config.dest.img));

	// copy all files from /src/fonts directory
	src(config.src.root + 'fonts/*.*')
		.pipe(newer(config.dest.css + 'fonts/'))
		.pipe(dest(config.dest.css + 'fonts/'));

	// copy all roots files except *.html
	src(config.src.root + '*.(!html)*')
		.pipe(dest(config.dest.root));

	// copy all files from /src/video directory
	src(config.src.root + 'video/*.*')
		.pipe(newer(config.dest.root + 'video/'))
		.pipe(dest(config.dest.root + 'video/'));

	// copy svg files
	src(config.src.img + '*.svg')
		.pipe(newer(config.dest.img))
		.pipe(dest(config.dest.img));

	done();
}

function copyWatch() {
	return watch([
		config.src.img + '*',
		config.src.root + 'fonts/*',
	], series(copy))
		.on('change', browserSync.reload);
}

module.exports = { copy, copyWatch };
